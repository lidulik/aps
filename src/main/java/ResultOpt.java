
import application.Controller;
import findOptimalStatistic.statistic.StatisticController;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ResultOpt {
    private final List<String[]> resultRow = new ArrayList<>();
    private Double maxFail = 1.0;
    private Double minWorkTime = 1.0;

    public void main2() {
        for (int countOfDevice = 1; countOfDevice < 11; countOfDevice++) {
            for (int countOfSizeBuffer = 1; countOfSizeBuffer < 11; countOfSizeBuffer++) {
                for (double time = 0; time < 4; time++) {
                    StatisticController.countOfClients = 15;
                    StatisticController.countOfRequiredRequests = 1000;
                    StatisticController.countOfDevices = countOfDevice;
                    StatisticController.sizeOfBuffer = countOfSizeBuffer;
                    StatisticController.minimum = time * 0.1;
                    StatisticController.maximum = (time + 1) * 0.1;
                    StatisticController.lambda = 2.5;
                    if (findResults()){
                        double price = 0;
                        price += countOfSizeBuffer * 1000;
                        switch ((int) time) {
                            case (0) -> price += countOfDevice * 10000;
                            case (1) -> price += countOfDevice * 5000;
                            case (2) -> price += countOfDevice * 3000;
                            case (3) -> price += countOfDevice * 1500;
                        }
                        this.resultRow.add(new String[]{String.valueOf(this.maxFail), String.valueOf(this.minWorkTime),
                                String.valueOf(time * 0.1), String.valueOf((time + 1) * 0.1),
                                String.valueOf(countOfDevice), String.valueOf(countOfSizeBuffer),
                                String.valueOf(price)});
                    }
                }
            }
        }
        JFrame currentFrame = new JFrame() {
        };
        currentFrame.setVisible(true);
        currentFrame.setTitle("Конфигурация");
        currentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        currentFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        String[] columnNames = {"Вероятность отказа","Загруженность прибора", "Минимальное время", "Максимальное время",
                "Кол-во тренеров", "Размер буфера", "Стоимость рабоч.смены"};

        String[][] data = new String[resultRow.size()][10];
        int i = 0;
        for (String[] str : resultRow) {
            data[i][0] = str[0];
            data[i][1] = str[1];
            data[i][2] = str[2];
            data[i][3] = str[3];
            data[i][4] = str[4];
            data[i][5] = str[5];
            data[i][6] = str[6];
            i++;
        }
        JTable table = new JTable(data, columnNames);
        JScrollPane scroll = new JScrollPane(table);
        table.setPreferredScrollableViewportSize(new Dimension(500, 900));
        currentFrame.getContentPane().add(scroll);
        currentFrame.revalidate();
    }

    public boolean findResults(){
        Controller controller = new Controller();
        controller.auto();
        int sum = 0;
        for (int i = 0; i < controller.getStatistics().getClientsCount(); i++) {
            sum += controller.getStatistics().getClientsStats().get(i).getCanceledTasksCount();
        }
        double workTime = 1.1;
        for (int i = 0; i < controller.getStatistics().getDevicesCount(); i++){
            double newWorkTime = controller.getStatistics().getDeviceTime()[i] / Controller.currentTime();
            if (workTime > newWorkTime) {
                workTime = newWorkTime;
            }
        }
        double maxFail = (double) sum / controller.getStatistics().getTotalRequestsCount();
        if (maxFail <= 0.1 && workTime >= 0.9){
            this.maxFail = maxFail;
            this.minWorkTime = workTime;
            return true;
        }
        return false;
    }
}
